import nasspace
import datasets
import random
import numpy as np
import torch
import os
from tqdm import trange
# from statistics import mean
import time
# import collections
# from utils import add_dropout
from operator import itemgetter
from args import get_args

args = get_args()
os.environ['CUDA_VISIBLE_DEVICES'] = args.GPU

# Reproducibility
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)

def get_batch_jacobian(net, x):
    net.zero_grad()

    x.requires_grad_(True)

    _, y = net(x)

    y.backward(torch.ones_like(y))
    jacob = x.grad.detach()

    return jacob#, grad


def eval_score_perclass(jacob, labels=None, n_classes=10):
    k = 1e-5
    #n_classes = len(np.unique(labels))
    per_class={}
    for i, label in enumerate(labels):
        if label in per_class:
            per_class[label] = np.vstack((per_class[label],jacob[i]))
        else:
            per_class[label] = jacob[i]

    ind_corr_matrix_score = {}
    for c in per_class.keys():
        s = 0
        try:
            corrs = np.corrcoef(per_class[c])

            s = np.sum(np.log(abs(corrs)+k))#/len(corrs)
            if n_classes > 100:
                s /= len(corrs)
        except: # defensive programming
            continue

        ind_corr_matrix_score[c] = s

    # per class-corr matrix A and B
    score = 0
    ind_corr_matrix_score_keys = ind_corr_matrix_score.keys()
    if n_classes <= 100:

        for c in ind_corr_matrix_score_keys:
            # B)
            score += np.absolute(ind_corr_matrix_score[c])
    else: 
        for c in ind_corr_matrix_score_keys:
            # A)
            for cj in ind_corr_matrix_score_keys:
                score += np.absolute(ind_corr_matrix_score[c]-ind_corr_matrix_score[cj])

        # should divide by number of classes seen
        score /= len(ind_corr_matrix_score_keys)

    return score


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
searchspace = nasspace.get_search_space(args, nasbench)
train_loader = datasets.get_data(args.dataset, args.data_loc, args.trainval, args.batch_size, args.augtype, args.repeat, args)
os.makedirs(args.save_loc, exist_ok=True)


times     = []
chosen    = []
acc       = []
val_acc   = []
topscores = []
order_fn = np.nanargmax


if args.dataset == 'cifar10':
    acc_type = 'ori-test'
    val_acc_type = 'x-valid'
else:
    acc_type = 'x-test'
    val_acc_type = 'x-valid'


C = C_AUX = args.C #200
P = P_AUX = args.P
S = args.S

times = []
samples = []
histories = []
best_arch = []
zero_proxy_times = []
best_arch_val_acc = []
runs = trange(args.n_runs, desc='acc: ')
for N in runs:
    scores = []
    samples.append([])
    zero_proxy_time = 0.0

    npstate = np.random.get_state()
    ranstate = random.getstate()
    torchstate = torch.random.get_rng_state()
    
    init_population = []
    total_time_cost = 0
    start = time.time()

    data_iterator = iter(train_loader)
    x, target = next(data_iterator)
    x, target = x.to(device), target.cpu().tolist()
    while len(init_population) < C:
        uid = searchspace.random_arch() # gt random id for arch
        #print(uid)
        uid = searchspace[uid] # for nasbench101 hash
        #print(uid)
        network = searchspace.get_network(uid) # get arch based on id
        #print(network)
        network.to(device)        

        jacobs_batch = get_batch_jacobian(network, x)
        jacobs_batch = jacobs_batch.reshape(jacobs_batch.size(0), -1).cpu().tolist()

        try:
            s = eval_score_perclass(jacobs_batch, target)

        except Exception as e:
            print(e)
            s = np.nan

        init_population.append((uid,s))
    proxy_time = (time.time()-start)
    zero_proxy_time += proxy_time
    total_time_cost += proxy_time # add time for all zero-cost proxy processings
    
    # remove C - (C-P)
    init_population = sorted(init_population, key=itemgetter(1), reverse=True)
    init_population = init_population[:P] 
    
    population = []
    history = []

    for ind in reversed(init_population):
        #print(ind)
        # simulate train and get results
        acc12, acc, train_time = searchspace.train_and_eval(ind[0],None, acc_type=acc_type)
        #ind_values = (ind[0],acc12,train_time) # uid, acc, time
        ind_values = (ind[0],acc12,train_time, acc, ind[1]) # uid, acc, time, pred_acc
        population.append(ind_values)
        history.append(ind_values)
        total_time_cost += train_time
    
    C_AUX = C #cyles
    while C_AUX >= 0:
        C_AUX -= 1
        sample = []
        #sample with replacement
        if args.sampling == "S": # sample S candidates
            while len(sample) < S: 
                # Inefficient, but written this way for clarity. In the case of neural
                # nets, the efficiency of this line is irrelevant because training neural
                # nets is the rate-determining step.
                candidate = random.choice(list(population))
                sample.append(candidate)
        elif args.sampling == "highest": # get the highest indv only
            sample.append(max(population, key=itemgetter(1))) #min value
        elif args.sampling == "lowest": # get the lowest indv only
            sample.append(min(population, key=itemgetter(1))) #min value

        parent = max(sample, key=lambda i: i[1]) #get highest acc

        start = time.time()
        generation = []
        P_AUX = P #population size
        #generate generation
        while len(generation) <= P_AUX:
            new_ind = searchspace.mutate_arch(parent[0]) # mutate parent
            new_ind = searchspace[new_ind] # for nasbench101 hash
            network = searchspace.get_network(new_ind).to(device) # get arch based on id

            jacobs_batch = get_batch_jacobian(network, x)
            jacobs_batch = jacobs_batch.reshape(jacobs_batch.size(0), -1).cpu().tolist()

            try:
                s = eval_score_perclass(jacobs_batch, target)
            except Exception as e:
                print(e)
                s = np.nan

            generation.append((new_ind,s))

        proxy_time = (time.time()-start)
        zero_proxy_time += proxy_time
        total_time_cost += proxy_time 
        
        chosen_ind = max(generation, key=lambda i: i[1]) #get highest score
        
        save_generation = []
        for _ind in generation:
            acc12, acc, train_time = searchspace.train_and_eval(_ind[0],None, acc_type=acc_type)
            save_generation.append([_ind[0], acc12, train_time, acc, _ind[1]])
        samples[N].append(save_generation)

        acc12, acc, train_time = searchspace.train_and_eval(chosen_ind[0],None, acc_type=acc_type)
        #ind_values = (chosen_ind[0],acc12,train_time)
        ind_values = (chosen_ind[0],acc12,train_time, acc, chosen_ind[1]) # uid, acc, time, pred_acc
        population.append(ind_values) # add ind to current pop
        history.append(ind_values) # add ind to history
        total_time_cost += train_time
        
        if args.regularize == 'oldest':
            indv = population[0] #oldest 
        elif args.regularize == 'lowest': # remove lowest scoring 
            indv = min(population, key=itemgetter(1)) #min value
        elif args.regularize == 'highest': # remove highest scoring 
            indv = max(population, key=itemgetter(1)) #min value
        population.pop(population.index(indv))

    times.append(total_time_cost)
    histories.append(history)
    zero_proxy_times.append(zero_proxy_time)
    
    top_scoring_arch = max(history, key=lambda i: i[1]) #i[0,1,2] = idx, acc12, time
    best_arch.append(searchspace.get_final_accuracy(top_scoring_arch[0], acc_type, False))
    if not args.dataset == 'cifar10' or args.trainval:
        best_arch_val_acc.append(searchspace.get_final_accuracy(top_scoring_arch[0], val_acc_type, args.trainval))

print(best_arch)

print(histories)
print(times)
print(zero_proxy_times)
print(f'Mean search time:{np.mean(times):.2f}+/-{np.std(times):.2f}')
if len(best_arch_val_acc) > 0:
    print(f'Mean val  acc:{np.mean(best_arch_val_acc):2.2f}+/-{np.std(best_arch_val_acc):2.2f}')
print(f'Mean test acc:{np.mean(best_arch):2.2f}+/-{np.std(best_arch):2.2f}')
print("Samples")
print(samples)
