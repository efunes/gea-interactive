import argparse


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='GEA')
    parser.add_argument('--data_loc', default='../cifardata/',
                        type=str, help='dataset folder')
    parser.add_argument('--api_loc', default='~/nas_benchmark_datasets/NAS-Bench-201-v1_1-096897.pth',
                        type=str, help='path to API')
    parser.add_argument('--save_loc', default='results/',
                        type=str, help='folder to save results')
    parser.add_argument('--nasspace', default='nasbench201',
                        type=str, help='the nas search space to use')
    parser.add_argument('--batch_size', default=256, type=int)
    parser.add_argument('--evaluate_size', default=256, type=int)
    parser.add_argument('--repeat', default=1, type=int,
                        help='how often to repeat a single image with a batch')
    parser.add_argument('--augtype', default='none',
                        type=str, help='which perturbations to use')
    parser.add_argument('--sigma', default=0.05, type=float,
                        help='noise level if augtype is "gaussnoise"')
    parser.add_argument('--GPU', default='0', type=str)
    parser.add_argument('--stem_out_channels', default=16, type=int,
                        help='output channels of stem convolution (nasbench101)')
    parser.add_argument('--num_stacks', default=3, type=int,
                        help='#stacks of modules (nasbench101)')
    parser.add_argument('--num_modules_per_stack', default=3,
                        type=int, help='#modules per stack (nasbench101)')
    parser.add_argument('--num_labels', default=1, type=int,
                        help='#classes (nasbench101)')
    parser.add_argument('--seed', default=1, type=int)
    parser.add_argument('--trainval', action='store_true')
    parser.add_argument('--dataset', default='cifar10', type=str)
    parser.add_argument('--n_samples', default=100, type=int)
    parser.add_argument('--n_runs', default=1, type=int)
    parser.add_argument('--regularize', default='oldest', type=str, help='which scheme to use to remove indvs from population',
                        choices=["oldest", "highest", "lowest"])
    parser.add_argument('--sampling', default='S', type=str, help='which scheme to use to sample candidates to be parent',
                        choices=["S", "highest", "lowest"])
    parser.add_argument('--C', default=200, type=int)
    parser.add_argument('--P', default=10, type=int)
    parser.add_argument('--S', default=5, type=int)

    args, argsv = parser.parse_known_args()
    
    return args
